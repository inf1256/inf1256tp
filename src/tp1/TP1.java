/**
 * 
 */
package tp1;

/**
 * @author Johnny Tsheke --UQAM
 * Une solution du TP1 Hiver 2017 
 *ATT: Ce n'est pas la seule solution mais une solution parmi d'autres!
 *Ceci est une version destinée aux débutants en programmation. ça n'utilise pas des méthodes (pas encore vue en classe)
 */
import java.util.*; //pouravoir la classe Scanner et le formatage de l'affichage
import java.lang.*; //pour avoir notamment la classe Math
public class TP1 {

	final static double TEMPS_TRAVAIL = 40.0; //constante
	final static String DELIMITEUR_SAISIE =System.lineSeparator(); //ou simplement "\n"; separateur de ligne
	final static int NOMBRE_MINUTES_PAR_HEURE = 60;
	public static void main(String[] args) {
		//déclaration et initialisation des variables
		int matricule = 0;
		String nom = "";
		String prenom = "";
		double nhBanque = 0.0;
		double nhProjet1 = 0.0;
		double nhProjet2 = 0.0;
		double nhProjet3 = 0.0;
		double nhProjet4 = 0.0;
		double nhTotal = 0.0;
		double nhAPayer = 0.0;
		double nhNouvelleBanque = 0.0;
		boolean saisieValide = false;
		boolean aTravaillerSurProjet = false;//par défault
		String patternNom="[a-zA-Z]+( [a-zA-Z]+)*";//ATTENTION espace entre ( et [ est important. sinon on reconnais pas des nom compose de plusieurs mot comme "De Gaulle"
		
		String patternOuiNon = "[oOnN]";
		String patternOui = "[oO]";
		String patternNon = "[nN]";
		
		String reponseSaisirFeuilleDeTemps = "";
		boolean reponseSaisirFeuilleDeTempsValide = false;
		
		//déclaration du scanner pour la saisie des données
		
		Scanner clavier = new Scanner(System.in);
		clavier.useLocale(Locale.US);//pour utiliser un les nombre reel avec un point ex: 3.5
		clavier.useDelimiter(DELIMITEUR_SAISIE);//une saisie par ligne pour permmetre par exemple de saisir des noms de plusieurs mots
		
		//Affichage du message de bienve
		
		System.out.println("Bienvenu sur le programme des feuilles de temps");
		System.out.print("Ce programme permet de saisir les information d'une feuille de temps à la fois");
		System.out.println("Par la suite le programme calcules les heures à payer et celles dans la nouvelle banque");
		
		//fin etape 0
		//début étape 1
		
		while(!reponseSaisirFeuilleDeTempsValide){
			//tant que la réponse pas valide
			System.out.println("Voulez-vous saisir une Feuille de Temps ? ");
			System.out.println("si Oui: saisir o ou O sinon, saisir n ou N");
			if(clavier.hasNext(patternOuiNon)){
				reponseSaisirFeuilleDeTempsValide = true; //pour arreter la boucle de validation si la bonne réponse est saisie
			}else{
				System.out.println("Réponse invalide");
				clavier.next();//déplacement de la tête de lecture
			}
		}
		//ici on a déjà une bonne réponse saisie
		reponseSaisirFeuilleDeTemps = clavier.next(patternOuiNon);//lecture de la réponse saisie
		
		if(reponseSaisirFeuilleDeTemps.equalsIgnoreCase("o")){
			//si la réponse est oui on va demander les information
			//-----------------------------------------
			//saisi et validation numéro matricule
			saisieValide = false;
			while(!saisieValide){
				System.out.println("Veuillez saisir le numero matricule employé svp--nombre entier");
				if(clavier.hasNextInt()){
					saisieValide =true;
				}else{
					System.out.println("Réponse invalide");
					clavier.next();//déplacement tête lecture pacq réponse invalide
				}
			}
			//ici la on a un numéro natricule valide à l'entrée
		matricule = clavier.nextInt();
		//-----------------------------------------
		//Saisie du nom
		saisieValide = false;
		while(!saisieValide){
			System.out.println("Veuillez saisir le nom de l'employé svp -- Chaine de caractèeres");
			if(clavier.hasNext(patternNom)){
				saisieValide =true;
			}else{
				System.out.println("Réponse invalide");
				clavier.next();//déplacement tête lecture pacq réponse invalide
			}
		}
		//ici la on a un nom valide à l'entrée
	 nom = clavier.next();
	//-----------------------------------------
	 //saisie du prénom --en réalité, c'est preque la même chose que le nom (copier coller puis adapter??? --avec intelligence biensur ...)
	 saisieValide = false;
		while(!saisieValide){
			System.out.println("Veuillez saisir le prénom de l'employé svp -- Chaine de caractèeres");
			if(clavier.hasNext(patternNom)){
				saisieValide =true;
			}else{
				System.out.println("Réponse invalide");
				clavier.next();//déplacement tête lecture pacq réponse invalide
			}
		}
		//ici la on a un prénom valide à l'entrée
	 prenom = clavier.next();
	//-----------------------------------------
	 //saisie du nombre d'heures en banque --un peu comme matricule mais le type double
	 
	 saisieValide = false;
		while(!saisieValide){
			System.out.println("Veuillez saisir le nombre d'heure en banque de l'employé svp--nombre réel");
			if(clavier.hasNextDouble()){
				saisieValide =true;
			}else{
				System.out.println("Réponse invalide");
				clavier.next();//déplacement tête lecture pacq réponse invalide
			}
		}
		//ici la on a un nombre d'heure valide à l'entrée
	nhBanque = clavier.nextDouble();
	
	//------------maintenat il faut demander les heures des projets ---
	//saisie du nombre d'heures projet 1 --un peu comme nombre heures en banque
	saisieValide = false;
	while(!saisieValide){
		System.out.println("Avez-vous travaillé sur proje 1? SiOui: o ou O, Sinon n ou N");
		if(clavier.hasNext(patternOuiNon)){
			saisieValide = true;
		}else{
			clavier.next();
		}
	}
	 //ici on a une réponse valide à l'entrée
	if(clavier.hasNext(patternNon)){//ici on deplace seulement la tête de lecture et on ne fait rien
		clavier.next();
	}else if(clavier.hasNext(patternOui)){
		//s'il a travaillé sur le projet on demande les heures comme avec les heures en banques
		clavier.next();//déplacement tête lecture
		saisieValide = false;
		while(!saisieValide){
			System.out.println("Veuillez saisir le nombre d'heure travailler par l'employé sur le projet 1 svp--nombre réel");
			if(clavier.hasNextDouble()){
				saisieValide =true;
			}else{
				System.out.println("Réponse invalide");
				clavier.next();//déplacement tête lecture pacq réponse invalide
			}
		}
		//ici la on a un nombre d'heure valide à l'entrée
	nhProjet1 = clavier.nextDouble();
	}
	//------------------
	//saisie du nombre d'heures projet 2 --un petit copie coller a partir de projet 1 permettrait de gagner du temmps ..
		saisieValide = false;
		while(!saisieValide){
			System.out.println("Avez-vous travaillé sur proje 2? SiOui: o ou O, Sinon n ou N");
			if(clavier.hasNext(patternOuiNon)){
				saisieValide = true;
			}else{
				clavier.next();
			}
		}
		 //ici on a une réponse valide à l'entrée
		
		if(clavier.hasNext(patternNon)){//ici on deplace seulement la tête de lecture et on ne fait rien
			clavier.next();
		}else if(clavier.hasNext(patternOui)){
			//s'il a travaillé sur le projet on demande les heures comme avec les heures en banques
			clavier.next();//déplacement tête lecture
			saisieValide = false;
			while(!saisieValide){
				System.out.println("Veuillez saisir le nombre d'heure travailler par l'employé sur le projet 2 svp--nombre réel");
				if(clavier.hasNextDouble()){
					saisieValide =true;
				}else{
					System.out.println("Réponse invalide");
					clavier.next();//déplacement tête lecture pacq réponse invalide
				}
			}
			//ici la on a un nombre d'heure valide à l'entrée
		nhProjet2 = clavier.nextDouble();
		}
		//------------------
		//saisie du nombre d'heures projet 3 --un petit copie coller a partir de projet 1 permettrait de gagner du temmps ..
			saisieValide = false;
			while(!saisieValide){
				System.out.println("Avez-vous travaillé sur proje 3? SiOui: o ou O, Sinon n ou N");
				if(clavier.hasNext(patternOuiNon)){
					saisieValide = true;
				}else{
					clavier.next();
				}
			}
			 //ici on a une réponse valide à l'entrée
			
			if(clavier.hasNext(patternNon)){//ici on deplace seulement la tête de lecture et on ne fait rien
				clavier.next();
			}else if(clavier.hasNext(patternOui)){
				//s'il a travaillé sur le projet on demande les heures comme avec les heures en banques
				clavier.next();//déplacement tête lecture
				saisieValide = false;
				while(!saisieValide){
					System.out.println("Veuillez saisir le nombre d'heure travailler par l'employé sur le projet 3 svp--nombre réel");
					if(clavier.hasNextDouble()){
						saisieValide =true;
					}else{
						System.out.println("Réponse invalide");
						clavier.next();//déplacement tête lecture pacq réponse invalide
					}
				}
				//ici la on a un nombre d'heure valide à l'entrée
			nhProjet3 = clavier.nextDouble();
			}
			//------------------
			//saisie du nombre d'heures projet 4 --un petit copie coller a partir de projet 1 permettrait de gagner du temmps ..
				saisieValide = false;
				while(!saisieValide){
					System.out.println("Avez-vous travaillé sur proje 4? SiOui: o ou O, Sinon n ou N");
					if(clavier.hasNext(patternOuiNon)){
						saisieValide = true;
					}else{
						clavier.next();
					}
				}
				 //ici on a une réponse valide à l'entrée
				if(clavier.hasNext(patternNon)){//ici on deplace seulement la tête de lecture et on ne fait rien
					clavier.next();
				}else if(clavier.hasNext(patternOui)){
					//s'il a travaillé sur le projet on demande les heures comme avec les heures en banques
					clavier.next();//déplacement tête lecture
					saisieValide = false;
					while(!saisieValide){
						System.out.println("Veuillez saisir le nombre d'heure travailler par l'employé sur le projet 4 svp--nombre réel");
						if(clavier.hasNextDouble()){
							saisieValide =true;
						}else{
							System.out.println("Réponse invalide");
							clavier.next();//déplacement tête lecture pacq réponse invalide
						}
					}
					//ici la on a un nombre d'heure valide à l'entrée
				nhProjet4 = clavier.nextDouble();
				}
	//----------------FIN saisie des info-------------------------
	//Étape 2 --Calculs
			nhTotal = nhBanque + nhProjet1 + nhProjet2 + nhProjet3 + nhProjet4;
			nhAPayer = Math.min(nhTotal, TEMPS_TRAVAIL);
			nhNouvelleBanque = nhTotal - nhAPayer;
			
	//Étape 3 -- Affichage ave formatage requis
			System.out.println("------------------");//pour séparés les info saissie de l'affichage du programme
			System.out.format("Numéro matricule employé :  %d %n", matricule);
			System.out.format("Nom employé :  %s %n", nom);
			System.out.format("Prénom employé :  %s %n", nom); //variable de travail
			double heuresEntieres = 0.0;
			double minutes = 0.0;
			//heure en banque
			heuresEntieres = Math.floor(nhBanque);
			minutes = (nhBanque - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heures banque :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			//on arrondi les minutes à aficher mais on affiche pas les parties décimales
			//heures projet 1
			heuresEntieres = Math.floor(nhProjet1);
			minutes = (nhProjet1 - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombrem heures Projet 1 :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			
			//heures projet 2
			heuresEntieres = Math.floor(nhProjet2);
			minutes = (nhProjet2 - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heure Projet 2 :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			
			//heures projet 3
			heuresEntieres = Math.floor(nhProjet3);
			minutes = (nhProjet3 - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heures Projet 3 :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			
			//heures projet 4
			heuresEntieres = Math.floor(nhProjet4);
			minutes = (nhProjet4 - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heures Projet 4 :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			
			//heures total
			heuresEntieres = Math.floor(nhTotal);
			minutes = (nhTotal - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heures Total :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			
			//heure projet 1
			heuresEntieres = Math.floor(nhAPayer);
			minutes = (nhAPayer - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heures à payer :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
			
			//heure projet 1
			heuresEntieres = Math.floor(nhNouvelleBanque);
			minutes = (nhNouvelleBanque - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
			System.out.format("Nombre heures nouvelle banque :  %.0f:%02d %n", heuresEntieres,Math.round(minutes));
		}
		//étape 4
		System.out.println("Fin du programme");
		

	}

}
